# doip-proxy

## DEPRECATED

The proxy functionality has been integrated in the [doip.js](https://codeberg.org/keyoxide/doipjs) project.

## Introduction

This is a proxy server written in Node.js for the [doip.js](https://js.doip.rocks/) library.

When doip.js is run in a Node.js environment, it can function completely
autonomously. When run in a browser, some proofs can't be verified due to CORS
restrictions or lack of Node.js tools like DNS resolvers. In these cases, the
requests will need to be routed to a proxy server running this project.

Currently, doip-proxy handles:
- REST queries (GET, result in JSON or text)
- DNS resolving
- XMPP vCard data fetching

Please note that XMPP functionality is disabled by default. Information on how
to enable it is found below.

## Getting started

### Installing the server using plain Node.js

```bash
git clone git@codeberg.org:keyoxide/doip-proxy.git
yarn --production
yarn start
```

Doip-proxy is now available at http://localhost:3000

### Installing the server using docker

```bash
git clone git@codeberg.org:keyoxide/doip-proxy.git
docker build -t doip-proxy .
docker container run -p 3000:3000 doip-proxy
```

Doip-proxy is now available at http://localhost:3000.

## API

### Get JSON document via REST

```text
/api/1/get/json/:url
```

The `url` needs to be encoded (in js: `encodeURIComponent(url)`).

### Get text document via REST

```text
/api/1/get/txt/:url
```

The `url` needs to be encoded (in js: `encodeURIComponent(url)`).

### Resolve DNS

```text
/api/1/get/dns/:hostname
```

`hostname` means just the domain without `https://`.

### Fetch XMPP vCard data

```text
/api/1/get/xmpp/:xmppid/:xmppdata
```

`xmppid` is also known as `jabberid` and looks like an email address:
`user@domain.org`.

`xmppdata` is the specific vCard entry that you are requesting. For DOIP proof
verification, you will need `DESC`. Possible values are:
`FN, NUMBER, USERID, URL, BDAY, NICKNAME, DESC`.

## Configuration

### Enabling XMPP functionality

Enabling XMPP functionality requires an XMPP account. Please note that after
some testing, it turns out not all XMPP instances work equally well with
doip-proxy. If XMPP continues to fail, please try a different XMPP instance.

XMPP account credentials are passed to the library using environment variables.

For example, with docker:

```bash
docker container run -p 3000:3000 -e XMPP_SERVICE=domain.org -e XMPP_USERNAME=username -e XMPP_PASSWORD=password doip-proxy
```

## Development

Todo:
- improve documentation
